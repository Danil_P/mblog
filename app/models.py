from app import db, bcrypt
import datetime

friends = db.Table('friends', 
            db.Column('myFriends_id', db.Integer, db.ForeignKey('user.id')),
            db.Column('IFriend_id', db.Integer, db.ForeignKey('user.id'))
      )

class User(db.Model):
      id = db.Column(db.Integer, primary_key = True);
      name = db.Column(db.String(20));
      email = db.Column(db.String(30), unique=True);
      photo = db.Column(db.String(100));
      password_hash = db.Column(db.String(255));
      posts = db.relationship('Post', backref='user', lazy='dynamic');
      active = db.Column(db.Boolean, default = True);
      friends = db.relationship('User', 
            secondary = friends,
            primaryjoin = (friends.c.myFriends_id == id),
            secondaryjoin = (friends.c.IFriend_id == id),
            backref = db.backref('followers', lazy = 'dynamic'),
            lazy = 'dynamic'
      );

      def __init__(self, *args, **kwargs):
            super(User, self).__init__(*args, **kwargs);
      
      #Login Interface
      def get_id(self):
            return str(self.id);
      
      def is_authenticated(self):
        return True;

      def is_active(self):
            return self.active;

      def is_anonymous(self):
            return False;

      #Work with password
      @staticmethod
      def make_password(plaintext):
            return bcrypt.generate_password_hash(plaintext);
      
      def check_password(self, raw_password):
            return bcrypt.check_password_hash(self.password_hash, raw_password);
      
      @classmethod
      def create(cls, email, password, **kwargs):
            return User(
                  email = email,
                  password_hash = User.make_password(password),
                  **kwargs
            );
      
      @staticmethod
      def authenticate(email, password):
            user = User.query.filter(User.email == email).first();
            if user and user.check_password(password):
                  return user;
            return False;


from app import login_manager;
@login_manager.user_loader
def _user_loader(user_id):
      return User.query.get(int(user_id));


class Post(db.Model):
      id = db.Column(db.Integer, primary_key = True);
      body = db.Column(db.Text);
      user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
      created_timestamp = db.Column(db.DateTime, default=datetime.datetime.now)

      def __init__(self, *args, **kwargs):
            super(Post, self).__init__(*args, **kwargs);
      
      def __repr__(self):
            return self.name;
