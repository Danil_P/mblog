import os;

from flask import g;
from flask_login import login_required
from flask import Blueprint, flash, redirect, render_template, request;
from flask import url_for;

from app.models import User, Post;
from app import app, db;
from users.forms import PostForm
users = Blueprint('users', __name__, template_folder='templates')

@users.route('/to_friend/<int:id>')
@login_required
def add_to_friends(id):
      user = User.query.get_or_404(id);
      if not user or g.user == user:
            return redirect('index.html');
      
      
      g.user.friends.append(user);
      
      db.session.add(g.user);
      db.session.commit();
      flash(" {0} added to friend list".format(user.name), "success")
      return redirect( url_for('users.profile', id = g.user.id));

@users.route('/<int:id>', methods=['GET', 'POST'])
def profile(id):
      user = User.query.get_or_404(id);
      if not user:
            return redirect('index.html');
      if g.user == user:
            if request.method == 'POST':
                  form = PostForm(request.form);
                  if form.validate():
                        post = Post();
                        form.populate_obj(post);
                        post.user = user;
                        db.session.add(post);
                        db.session.commit();
                        return redirect(url_for('users.profile', id=id));
            else:
                  return render_template("users/profile.html",
                                                user = user,
                                                form = PostForm()
                                          );
      return render_template('users/profile.html', user = user);

